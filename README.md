# Inclumsy
- A mod that spoof user's ping to the server in Minecraft aka ping spoofer.

<br>

## Usage
- `/inclumsy gui` - Open the settings GUI.

<br>

## Installation
1. Download the [Inclumsy](#download) mod.
2. Place the jar in your Weave mods folder.
    1. Windows: `%userprofile%\.weave\mods`
    2. Unix: `~/.weave/mods`

<br>

## Build
1. Clone the repository.
2. Run `./gradlew build` in the root directory.
3. The built jar file will be in `build/libs`.

<br>

## Credits
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.

<br>

## Download
- [Package Registry](https://gitlab.com/candicey-weave/inclumsy/-/packages) - Select the latest version and download the jar file that ended with `-relocated.jar`.

<br>

## License
- This project is licensed under the [GNU General Public License Version 3](LICENSE).
