package com.gitlab.candicey.inclumsy.command

import com.gitlab.candicey.inclumsy.commandInitialisationData
import com.gitlab.candicey.zenithcore.versioned.v1_8.command.CommandAbstract

open class Entry : CommandAbstract(commandInitialisationData)